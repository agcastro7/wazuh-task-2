=== Wazuh MD5 Checker ===
Tags: md5, hash, wazuh
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Wazuh MD5 Checker is a WordPress plugin built for Wazuh technical task #2.

== Description ==

Wazuh MD5 Checker generates a simple form, where the user can check the correctness of a list of MD5 hashes.

== Installation ==

To install this plugin using FTP:

1. Upload the directory `wz-md5` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

To install this plugin using Wordpress Administration page:

1. In the Plugins section, select Add new
2. Select the Upload plugin option
3. Select the `wz-md5.zip` file and press Install now
4. Activate the plugin through the 'Plugins' menu in WordPress

== How to use ==

The plugin creates the shortcode ```wz_md5```. When inserted in a post, it displays a form with a text area, where you can insert several lines with the following format:

```
ID NAME MD5_HASH
```

After submitting the form using the "Check MD5" button, the plugin will generate a hash for every ID-NAME pair. It will store it in the database if it's not already there, and after that it will compare it to the MD5_HASH provided. The plugin output will report which hashes were correct and which ones were not.