<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.mariagomezdev.org
 * @since             1.0.0
 * @package           Wz_Md5
 *
 * @wordpress-plugin
 * Plugin Name:       Wazuh MD5 Checker
 * Plugin URI:        https://bitbucket.org/agcastro7/wazuh-task-2
 * Description:       MD5 checker for Wazuh technical task 2.
 * Version:           1.0.0
 * Author:            Maria Gomez
 * Author URI:        www.mariagomezdev.org
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wz-md5
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WZ_MD5_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wz-md5-activator.php
 */
function activate_wz_md5() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wz-md5-activator.php';
	Wz_Md5_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wz-md5-deactivator.php
 */
function deactivate_wz_md5() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wz-md5-deactivator.php';
	Wz_Md5_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wz_md5' );
register_deactivation_hook( __FILE__, 'deactivate_wz_md5' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wz-md5.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wz_md5() {

	$plugin = new Wz_Md5();
	$plugin->run();

}
run_wz_md5();
