<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @link       www.mariagomezdev.org
 * @since      1.0.0
 *
 * @package    Wz_Md5
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

global $wpdb;
$table = $wpdb->prefix.'wz_md5';

// delete option
delete_option('WZ_MD5_VERSION');

// drop db table
$wpdb->query("DROP TABLE IF EXISTS $table");