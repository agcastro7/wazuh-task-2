
(function( $ ) {

	/**
	 * Display a message box in the view
	 * @param {string} message Message to be displayed (admits HTML code)
	 * @param {boolean} error True if it is an error message, false otherwise
	 */
	function displayMessage(message, error = false){
		$("#message").removeClass("wz-hidden");
		if(error){
			$("#message").addClass("wz-error");
		}
		else{
			$("#message").addClass("wz-info");
		}
		$("#message").html(message);
	}

	/**
	 * Hide the message box from the view
	 */
	function hideMessage(){
		$("#message").addClass("wz-hidden");
		$("#message").removeClass("wz-error");
		$("#message").removeClass("wz-info");
	}

	/**
	 * Parse user input into an array of JS objects, and show error messages if needed
	 * @param {string} input User input
	 */
	function parseInput(input){

		if(input == ""){
			displayMessage("Input can't be empty!", true);
			return false;
		}

		// Delete empty lines
		const lines = input.split('\n').filter((line) => line.length > 0);

		for (let i = 0; i < lines.length; i++) {
			const line = lines[i];
			if(line.split(" ").length != 3){
				displayMessage(`Incorrect format on line ${i+1} <br>Input should be formatted like this: <br>ID NAME MD5`, true);
				return false;
			}
		}

		const md5s = lines.map(
			(line) => {
				const words = line.split(" ");
				const md5 = {
					id: words[0],
					name: words[1],
					hash: words[2]
				};
				return md5;
			});

		return md5s;
	}

	/**
	 * Toggle loading state for submit button
	 */
	function toggleLoading(){
		$(".wz-spinner").toggleClass("wz-hidden");
	}

	/**
	 * Send user data using an AJAX request
	 * @param {array} md5s Array of JS Objects containing user input
	 */
	function sendData(md5s){

		toggleLoading();

		const data = {
			action: "wz_md5_check",
			mode: "POST",
			md5s: JSON.stringify(md5s)
		};

		$.ajax({
			type: "POST",
			url: wp_ajax.url, // wp_ajax is the variable sent via wp_localize_script
			data: data,
			error: function(response){
				toggleLoading();
				displayMessage("There was an error", true);
			},
			success: function(response){
				toggleLoading();
				const results = JSON.parse(response);
				showResults(results);
			}
		});
	}

	/**
	 * Show the checking results in the view.
	 * @param {array} results Check results. It must be an array of JS objects, one for
	 *                        each user input line, containing two fields: the hash ID
	 *                        and a boolean value indicating wether the MD5 was correct
	 *                        or not.
	 */
	function showResults(results){
		let resultsMsg = "";
		for (let i = 0; i < results.length; i++) {
			const result = results[i];
			if(result.correct){
				resultsMsg += `Hash number ${result.id} is correct <br>`;
			}
			else{
				resultsMsg += `Hash number ${result.id} is not correct <br>`;
			}
		}
		displayMessage(resultsMsg);
	}

	$(function() {
		$("#form").submit( function(event){
			hideMessage();
			const input = $("#text").val();
			const md5s = parseInput(input);
			if(md5s){
				sendData(md5s);
			}
			event.preventDefault();
		});
	});

})( jQuery );
