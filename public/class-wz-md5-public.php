<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.mariagomezdev.org
 * @since      1.0.0
 *
 * @package    Wz_Md5
 * @subpackage Wz_Md5/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wz_Md5
 * @subpackage Wz_Md5/public
 * @author     Maria Gomez <contacto@mariagomezdev.org>
 */
class Wz_Md5_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The shortcode used for rendering the form.
	 * 
	 * @since    1.0.0.
	 * @access   private
	 * @var      string    $shortcode      The shortcode used for rendering the form.
	 */
	private $shortcode;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->shortcode = 'wz_md5';
		$this->create_shortcode();
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wz-md5-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wz-md5-public.js', array( 'jquery' ), $this->version, false );

		// Send AJAX URL to JS script
		wp_localize_script($this->plugin_name, 'wp_ajax', array(
			'url' => admin_url( 'admin-ajax.php' )
		));

	}

	/**
	 * Add the shortcode for displaying the form.
	 * 
	 * @since    1.0.0.
	 */
	public function create_shortcode() {
		add_shortcode($this->shortcode, array($this,'render_shortcode_template'));
	}

	/**
	 * Render the form template when the shortcode is used.
	 * 
	 * @since    1.0.0.
	 */
	public function render_shortcode_template($atts) {
		ob_start();
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/wz-md5-public-display.php';
		return ob_get_clean();
	}

	/**
	 * This functions receives the user input through an AJAX request and
	 * performs the operations described in the task.
	 * 
	 * @since    1.0.0.
	 */
	public function check_md5s(){

		if(isset($_REQUEST["md5s"])){
			// Preprocess data to remove escape bars
			$request_data = str_replace("\\","",$_REQUEST["md5s"]);

			$md5s = json_decode($request_data);
			$response = array();

			foreach ($md5s as $md5) {
				$key = $md5->id . $md5->name;
				$hash = md5($key);

				global $wpdb;
				$table_name = $wpdb->prefix . 'wz_md5';

				$results = $wpdb->get_results(
					$wpdb->prepare(
						"SELECT * FROM {$table_name} WHERE `hash` = '%s';",
						$hash
					)
				);

				// If the md5 is not yet in the database, insert it
				if(count($results) == 0){
					$wpdb->query(
						$wpdb->prepare(
							"INSERT INTO {$table_name} VALUES('%s','%s','%s');",
							$md5->id,
							$md5->name,
							$hash
						)
					);
				}

				// Check wether the hash is correct or not
				$response[] = array(
					"id" => $md5->id,
					"correct" => $hash == $md5->hash
				);
			}
			echo json_encode($response);
		}
		die();
	}

}
