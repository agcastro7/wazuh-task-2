# Wazuh MD5 Checker plugin

Wazuh MD5 checker is a Wordpress plugin written for the Wazuh technical task #1.

## How to install

1. Download plugin file
2. In your Wordpress Admin Panel, go to Plugins / Add New / Upload Plugin
3. Select the plugin file and click Install Now
4. Activate the plugin

## How to use

1. In a Wordpress post, insert the shortcode `[wz_md5]`.
2. Open or preview the post to see the plugin input form.
3. You can input several hashes in the text area, each one in a different line, using he format:
```
ID NAME MD5_HASH
```
4. Click on the MD5 Check button.
5. The results will be displayed in a message box.
  
## Live demo

[See live demo](http://s791075640.mialojamiento.es/)