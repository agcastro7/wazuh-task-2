<?php

/**
 * Fired during plugin activation
 *
 * @link       www.mariagomezdev.org
 * @since      1.0.0
 *
 * @package    Wz_Md5
 * @subpackage Wz_Md5/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wz_Md5
 * @subpackage Wz_Md5/includes
 * @author     Maria Gomez <contacto@mariagomezdev.org>
 */
class Wz_Md5_Activator {

	/**
	 * Sets up Wordpress after the plugin has been activated.
	 *
	 * Creates database table for storing hashes.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		flush_rewrite_rules();
		
		global $wpdb;
		$table_name = $wpdb->prefix . 'wz_md5';

		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id VARCHAR(3) NOT NULL,
			name VARCHAR(200) NOT NULL,
			hash VARCHAR(32) NOT NULL,
			UNIQUE KEY id (id)
		);";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}

}
