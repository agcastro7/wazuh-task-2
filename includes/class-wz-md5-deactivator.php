<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.mariagomezdev.org
 * @since      1.0.0
 *
 * @package    Wz_Md5
 * @subpackage Wz_Md5/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wz_Md5
 * @subpackage Wz_Md5/includes
 * @author     Maria Gomez <contacto@mariagomezdev.org>
 */
class Wz_Md5_Deactivator {

	/**
	 * Cleans up after deactivation.
	 *
	 * Lorem ipsum dolor sit amet consectetur adipisicing.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		flush_rewrite_rules();
	}

}
