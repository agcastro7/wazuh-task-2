<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       www.mariagomezdev.org
 * @since      1.0.0
 *
 * @package    Wz_Md5
 * @subpackage Wz_Md5/admin/partials
 */
?>
<h1>How to use Wazuh MD5 Checker</h1>

<h3>Step 1: Shortcode</h3>

Insert the following shortcode in a page or post:

<pre><code>[wazuh_md5]</code></pre>

<h3>Step 2: Filling the form</h3>

<p>Navigate to your page or post and it will display the following form:</p>

<img src="<?php echo plugin_dir_url(  __FILE__  ) . '/form1.png'?>" alt="Form screenshot">

<p>Insert your list of items. Each item must be in a different line, and the text format must be:</p>

<pre><code>ID NAME MD5</code></pre>

<h3>Step 3: Submit the form</h3>

<p>Click on the <strong>Check MD5</strong> button. The plugin will submit your information, and display the results in a message box:</p>


<img src="<?php echo plugin_dir_url(  __FILE__  ) . '/form2.png'?>" alt="Form screenshot">