<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.mariagomezdev.org
 * @since      1.0.0
 *
 * @package    Wz_Md5
 * @subpackage Wz_Md5/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wz_Md5
 * @subpackage Wz_Md5/admin
 * @author     Maria Gomez <contacto@mariagomezdev.org>
 */
class Wz_Md5_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wz-md5-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wz-md5-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Create the admin page
	 * 
	 * @since    1.0.0
	 */
	public function create_admin_page() {
		add_menu_page( 'How to use Wazuh MD5 Checker', 'Wazuh MD5', 'manage_options', 'wazuh-md5', array($this, 'render_admin_page') , 'dashicons-admin-network' );
	}

	/**
	 * Render admin section
	 * 
	 * @since    1.0.0
	 */
	public function render_admin_page(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/wz-md5-admin-display.php';
	}

}
